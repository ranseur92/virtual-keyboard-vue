import defaultLocales from "./defaultLocales";

export default {
	props: {
		locales: {
			type: [Object, Function],
			default: undefined
		},
		locale: {
			type: String,
			default: "en"
		},
		locale_fallback: {
			type: String,
			default: "en"
		}
	},
	computed: {
		vk_locales() {
			if (typeof this.locales === "function") {
				return this.locales(defaultLocales);
			} else {
				return Object.assign(defaultLocales, this.locales);
			}
		}
	},
	methods: {
		locale_prop(key, fallback = undefined) {
			let currentLocale = this.vk_locales[this.locale];
			if ((currentLocale || {})[key] !== undefined) {
				return currentLocale[key];
			}
			return this.fallback_locale_prop(key, fallback);
		},
		fallback_locale_prop(key, fallback = undefined) {
			let fallbackLocale = this.vk_locales[this.locale_fallback];
			if ((fallbackLocale || {})[key] !== undefined) {
				return fallbackLocale[key];
			}
			return fallback;
		}
	}
}