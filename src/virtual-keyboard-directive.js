export default function (input, binding) {
	input.setAttribute('keyboard-controlled', '');
	input.setAttribute('keyboard-layout', getLayout(input));

	if (input.type === 'email') {
		input.setAttribute('type', 'text');
	}

	if (binding.modifiers.lock) {
		input.addEventListener('mousedown', (e) => {
			e.preventDefault();
			input.focus();
		});

		input.addEventListener('keydown', (e) => {
			e.preventDefault();
		});

		input.addEventListener('focus', () => {
			const pos = input.value.length;
			input.setSelectionRange(pos, pos);
		});
	}
}

function getLayout(input) {
	switch (input.type) {
		case "time":
		case "week":
		case "date":
		case "datetime-local":
			return "numeric";
		case "month":
			return "alpha-numeric";
		case "password":
		case "search":
		case "url":
			return "text";
		case "number":
			if (input.step && input.step.indexOf('0.') === 0)
				return "decimal";
			return "numeric";
		default:
			return input.type;
	}
}